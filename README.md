
# Ecommerce Admin

This is a simple admin and client side of Ecommerce.


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/fardinOA/e-commerce-repliq
```

Go to the project directory

```bash
  cd e-commerce-repliq
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run start
```


## How to use

 


```bash
  / for client
```
```bash
  /admin for admin
```


"use client";
import React, { createContext } from "react";
import useContextDataProvider from "./contextHoocks/useContextDataProvider";

interface CartItem {
    image: string;
    name: string;
    category: string;
    price: string | number;
}

interface ContextData {
    cart: CartItem[];
    setCart: React.Dispatch<React.SetStateAction<CartItem[]>>;
}

export const DataContext = createContext<ContextData>({
    cart: [],
    setCart: function (value: React.SetStateAction<CartItem[]>): void {
        throw new Error("Function not implemented.");
    },
});

const DataProvider = ({ children }: { children: React.ReactNode }) => {
    const allContexts = useContextDataProvider();
    return (
        <DataContext.Provider value={allContexts}>
            {children}
        </DataContext.Provider>
    );
};

export default DataProvider;

import { createContext, useState } from "react";

interface CartItem {
    image: string;
    name: string;
    category: string;
    price: string | number;
}

export interface ContextData {
    cart: CartItem[];
    setCart: React.Dispatch<React.SetStateAction<CartItem[]>>;
}

const useContextDataProvider = (): ContextData => {
    const [cart, setCart] = useState<CartItem[]>([]);
    return {
        cart,
        setCart,
    };
};

export default useContextDataProvider;

"use client";
import React, { useContext } from "react";
import { DataContext } from "../DataProvider";

const useContextData = () => {
    const data = useContext(DataContext);
    return data;
};

export default useContextData;

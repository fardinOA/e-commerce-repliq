/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ["*", "img.freepik.com", "etimg.etb2bimg.com"],
    },
};

module.exports = nextConfig;

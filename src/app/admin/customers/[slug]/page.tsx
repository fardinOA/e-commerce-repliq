import React from "react";
import CustomerDetails from "@/Components/Admin/Customers/CustomerDetails";

const page = () => {
    return <CustomerDetails />;
};

export default page;

import CategoryHighlighter from "@/Components/Client/CategoryHighlighter";
import ImageSlider from "@/Components/Client/ImageSlider";
import Image from "next/image";

const smartPhones = [
    {
        image: "https://img.freepik.com/free-vector/realistic-display-smartphone-with-different-apps_52683-30241.jpg",
        name: "App 1",
        category: "Social Media",
        price: "Free",
    },
    {
        image: "https://img.freepik.com/free-vector/realistic-display-smartphone-with-different-apps_52683-30241.jpg",
        name: "App 2",
        category: "Productivity",
        price: "$4.99",
    },
    {
        image: "https://img.freepik.com/free-vector/realistic-display-smartphone-with-different-apps_52683-30241.jpg",
        name: "App 3",
        category: "Entertainment",
        price: "Free",
    },
    {
        image: "https://img.freepik.com/free-vector/realistic-display-smartphone-with-different-apps_52683-30241.jpg",
        name: "App 4",
        category: "Gaming",
        price: "$2.99",
    },
    {
        image: "https://img.freepik.com/free-vector/realistic-display-smartphone-with-different-apps_52683-30241.jpg",
        name: "App 1",
        category: "Social Media",
        price: "Free",
    },
];
const sportsCategories = [
    {
        name: "Football",
        image: "https://etimg.etb2bimg.com/photo/74881928.cms",

        price: 100,
        category: "Outdoor",
    },
    {
        name: "Basketball",
        image: "https://etimg.etb2bimg.com/photo/74881928.cms",

        price: 80,
        category: "Indoor",
    },
    {
        name: "Tennis",
        image: "https://etimg.etb2bimg.com/photo/74881928.cms",

        price: 120,
        category: "Outdoor",
    },
    {
        name: "Cricket",
        image: "https://etimg.etb2bimg.com/photo/74881928.cms",

        price: 90,
        category: "Outdoor",
    },
    {
        name: "Tennis",
        image: "https://etimg.etb2bimg.com/photo/74881928.cms",

        price: 120,
        category: "Outdoor",
    },
];

export default function Home() {
    return (
        <div className=" container p-8 mx-auto   ">
            <ImageSlider />

            <div className=" space-y-6 ">
                <div className=" container p-6 mx-auto space-y-4 bg-white rounded-md  ">
                    <div className="flex justify-between">
                        <h3>
                            Grab the best deal on <span>Smartphone</span>
                        </h3>
                        <p>
                            View all <span>{">"}</span>
                        </p>
                    </div>
                    <hr />
                    <div>
                        <CategoryHighlighter array={smartPhones} />
                    </div>
                </div>

                <div className=" container p-6 mx-auto space-y-4 bg-white rounded-md  ">
                    <div className="flex justify-between">
                        <h3>
                            Grab the best deal on <span>Sports Items</span>
                        </h3>
                        <p>
                            View all <span>{">"}</span>
                        </p>
                    </div>
                    <hr />
                    <div>
                        <CategoryHighlighter array={sportsCategories} />
                    </div>
                </div>
            </div>
        </div>
    );
}

"use client";
import Link from "next/link";
import "./globals.css";
import { Inter } from "next/font/google";
import LeftSidebar from "@/Components/Admin/LeftSidebar";
import { useParams, usePathname } from "next/navigation";
import Navbar from "@/Components/Client/Navbar";
import DataProvider from "../../services/context/DataProvider";
const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    const url = usePathname();

    return (
        <html lang="en">
            <>
                <body>
                    {url.includes("/admin") ? (
                        <div>{children}</div>
                    ) : (
                        <div className="drawer">
                            <input
                                id="my-drawer-3"
                                type="checkbox"
                                className="drawer-toggle"
                            />
                            <div className="drawer-content flex flex-col">
                                {/* Client Navbar */}

                                <Navbar />

                                {/* Page content here */}
                                <DataProvider> {children}</DataProvider>
                            </div>
                            <div className="drawer-side">
                                <label
                                    htmlFor="my-drawer-3"
                                    className="drawer-overlay"
                                ></label>
                                <ul className="menu p-4 w-80 h-full bg-base-200">
                                    {/* Sidebar content here */}
                                    <li>
                                        <Link href="/cart">Cart</Link>
                                    </li>
                                    <li>
                                        <Link href="#">Account</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    )}
                </body>
            </>
        </html>
    );
}

"use client";
import Image from "next/image";
import React from "react";
import useContextData from "../../../../services/context/contextHoocks/useContextData";
interface ArrayInterface {
    image: string;
    name: string;
    category: string;
    price: string | number;
}

interface CategoryHighlighterProps {
    array: ArrayInterface[];
}
const CategoryHighlighter: React.FC<CategoryHighlighterProps> = ({ array }) => {
    const { setCart } = useContextData();

    const addToCart = (obj: ArrayInterface) => {
        console.log(obj);
        setCart((prev: ArrayInterface[]) => [...prev, obj]);
    };
    return (
        <div className=" flex justify-around flex-wrap  gap-6  ">
            {array.map((ele, ind) => (
                <div
                    key={ind}
                    className="card  w-[15rem] h-[18rem] bg-base-100 shadow-xl cursor-pointer hover:-translate-y-1 transition-all "
                >
                    <figure className=" h-[50%] ">
                        <Image
                            src={ele.image}
                            alt="Shoes"
                            height={20}
                            width={20}
                            layout="responsive"
                            className="object-contain h-full w-full"
                        />
                    </figure>
                    <div className="card-body">
                        <h2 className="card-title">
                            {ele.category}
                            <div className="badge badge-secondary">NEW</div>
                        </h2>
                        <p>{ele.name}</p>
                        <p>Price: {ele.price}</p>
                        <div className="card-actions flex flex-row justify-between">
                            <button
                                onClick={() => addToCart(ele)}
                                className="badge badge-outline"
                            >
                                Add to cart
                            </button>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default CategoryHighlighter;

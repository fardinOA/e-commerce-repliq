"use client";
import Link from "next/link";
import { useParams } from "next/navigation";
import React from "react";
const productsArray = [
    {
        name: "T-Shirt",
        color: "Blue",
        category: "Clothing",
        price: 19.99,
        slug: "t-shirt",
    },
    {
        name: "Sneakers",
        color: "Black",
        category: "Footwear",
        price: 79.99,
        slug: "sneakers",
    },
    {
        name: "Backpack",
        color: "Gray",
        category: "Accessories",
        price: 39.99,
        slug: "backpack",
    },
    {
        name: "Smartphone",
        color: "Silver",
        category: "Electronics",
        price: 599.99,
        slug: "smartphone",
    },
];

const ProductDetails = () => {
    const params = useParams();
    const product = productsArray.find((item) => item.slug === params?.slug);

    return (
        <div class="min-h-screen flex items-center justify-center bg-gray-100">
            <div class="max-w-md w-full bg-white rounded-lg shadow-md p-8">
                <h2 class="text-2xl font-bold mb-4">{product.name}</h2>
                <p class="text-gray-600 mb-4">
                    <span class="font-bold">Color:</span> {product.color}
                </p>
                <p class="text-gray-600 mb-4">
                    <span class="font-bold">Category:</span> {product.category}
                </p>
                <p class="text-gray-600 mb-4">
                    <span class="font-bold">Price:</span> ${product.price}
                </p>

                <Link
                    href="/admin/products"
                    class="mt-4 block text-center text-blue-500 hover:underline"
                >
                    Back to Products
                </Link>
            </div>
        </div>
    );
};

export default ProductDetails;

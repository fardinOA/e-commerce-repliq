"use client";
import Link from "next/link";
import { useParams } from "next/navigation";
import React from "react";
const customersArray = [
    {
        name: "John Doe",
        email: "johndoe@example.com",
        age: 30,
        address: "123 Main Street",
    },
    {
        name: "Jane Smith",
        email: "janesmith@example.com",
        age: 25,
        address: "456 Elm Street",
    },
    {
        name: "David Johnson",
        email: "davidjohnson@example.com",
        age: 35,
        address: "789 Oak Street",
    },
    {
        name: "Sarah Williams",
        email: "sarahwilliams@example.com",
        age: 28,
        address: "567 Pine Street",
    },
];

const CustomerDetails = () => {
    const params = useParams();

    const customer = customersArray.find(
        (item) => item.name == params?.slug.split("%20").join(" ")
    );

    return (
        <div class="min-h-screen flex items-center justify-center bg-gray-100">
            <div class="max-w-md w-full bg-white rounded-lg shadow-md p-8">
                <h2 class="text-2xl font-bold mb-4">{customer?.name}</h2>
                <p class="text-gray-600 mb-4">
                    <span class="font-bold">email:</span> {customer?.email}
                </p>
                <p class="text-gray-600 mb-4">
                    <span class="font-bold">age:</span> {customer?.age}
                </p>
                <p class="text-gray-600 mb-4">
                    <span class="font-bold">address:</span> ${customer?.address}
                </p>

                <Link
                    href="/admin/customers"
                    class="mt-4 block text-center text-blue-500 hover:underline"
                >
                    Back to customers
                </Link>
            </div>
        </div>
    );
};

export default CustomerDetails;

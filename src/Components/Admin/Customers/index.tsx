import Link from "next/link";
import React from "react";

const customersArray = [
    {
        name: "John Doe",
        email: "johndoe@example.com",
        age: 30,
        address: "123 Main Street",
    },
    {
        name: "Jane Smith",
        email: "janesmith@example.com",
        age: 25,
        address: "456 Elm Street",
    },
    {
        name: "David Johnson",
        email: "davidjohnson@example.com",
        age: 35,
        address: "789 Oak Street",
    },
    {
        name: "Sarah Williams",
        email: "sarahwilliams@example.com",
        age: 28,
        address: "567 Pine Street",
    },
];
const Customers = () => {
    return (
        <div className="   p-6 container mx-auto ">
            {/* add customer */}
            <label htmlFor="my_modal_6" className="btn btn-sm btn-primary my-4">
                Add customer
            </label>
            <input type="checkbox" id="my_modal_6" className="modal-toggle" />
            <div className="modal">
                <div className="modal-box">
                    <h3 className="font-bold text-lg">Add customer</h3>
                    <div>Here goes the add customer form</div>
                    <div className="modal-action">
                        <label
                            htmlFor="my_modal_6"
                            className="btn btn-sm btn-secondary my-4"
                        >
                            Close!
                        </label>
                    </div>
                </div>
            </div>
            {/*  */}
            <div className="relative !overflow-x-auto">
                <table className="table xl:table-fixed w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" className="px-6 py-3">
                                customer name
                            </th>
                            <th scope="col" className="px-6 py-3">
                                <div className="flex items-center">
                                    Email
                                    <a href="#">
                                        <svg
                                            className="w-3 h-3 ml-1.5"
                                            aria-hidden="true"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="currentColor"
                                            viewBox="0 0 24 24"
                                        >
                                            <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                                        </svg>
                                    </a>
                                </div>
                            </th>
                            <th scope="col" className="px-6 py-3">
                                <div className="flex items-center">
                                    Age
                                    <a href="#">
                                        <svg
                                            className="w-3 h-3 ml-1.5"
                                            aria-hidden="true"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="currentColor"
                                            viewBox="0 0 24 24"
                                        >
                                            <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                                        </svg>
                                    </a>
                                </div>
                            </th>
                            <th scope="col" className="px-6 py-3">
                                <div className="flex items-center">
                                    Address
                                    <a href="#">
                                        <svg
                                            className="w-3 h-3 ml-1.5"
                                            aria-hidden="true"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="currentColor"
                                            viewBox="0 0 24 24"
                                        >
                                            <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                                        </svg>
                                    </a>
                                </div>
                            </th>
                            <th scope="col" className="px-6 py-3">
                                <span className="sr-only">Edit</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {customersArray?.map((ele, ind) => (
                            <tr key={ind} className="bg-white dark:bg-gray-800">
                                <th
                                    scope="row"
                                    className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                                >
                                    {ele.name}
                                </th>
                                <td className="px-6 py-4">{ele.email}</td>
                                <td className="px-6 py-4">{ele.age}</td>
                                <td className="px-6 py-4">${ele.address}</td>
                                <td className="px-6 py-4 flex justify-around text-right">
                                    <p className="font-medium text-blue-600 dark:text-blue-500 hover:underline">
                                        delete
                                    </p>
                                    <p className="font-medium text-blue-600 dark:text-blue-500 hover:underline">
                                        Edit
                                    </p>
                                    <Link
                                        href={`/admin/customers/${ele.name}`}
                                        className="font-medium text-blue-600 dark:text-blue-500 hover:underline"
                                    >
                                        View
                                    </Link>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default Customers;

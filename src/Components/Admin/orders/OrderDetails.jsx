"use client";
import Link from "next/link";
import { useParams } from "next/navigation";
import React from "react";
const orderArray = [
    {
        id: 1,
        customer: "John Doe",
        product: "T-Shirt",
        quantity: 2,
        totalPrice: 39.98,
        status: "Pending",
        shippingAddress: "123 Main Street",
        orderDate: "2023-07-01",
        paymentMethod: "Credit Card",
        shippingMethod: "Express Shipping",
        isGift: false,
    },
    {
        id: 2,
        customer: "Jane Smith",
        product: "Sneakers",
        quantity: 1,
        totalPrice: 79.99,
        status: "Shipped",
        shippingAddress: "456 Elm Street",
        orderDate: "2023-06-29",
        paymentMethod: "PayPal",
        shippingMethod: "Standard Shipping",
        isGift: true,
    },
    {
        id: 3,
        customer: "David Johnson",
        product: "Backpack",
        quantity: 1,
        totalPrice: 39.99,
        status: "Delivered",
        shippingAddress: "789 Oak Street",
        orderDate: "2023-06-27",
        paymentMethod: "Credit Card",
        shippingMethod: "Express Shipping",
        isGift: false,
    },
    {
        id: 4,
        customer: "Sarah Williams",
        product: "Smartphone",
        quantity: 1,
        totalPrice: 599.99,
        status: "Pending",
        shippingAddress: "567 Pine Street",
        orderDate: "2023-06-25",
        paymentMethod: "Credit Card",
        shippingMethod: "Express Shipping",
        isGift: false,
    },
];

const OrderDetails = () => {
    const params = useParams();
    const order = orderArray.find((item) => item.id == params?.slug);

    return (
        <div className="flex p-8 container mx-auto flex-col items-center min-h-screen   py-8">
            <div className="w-full max-w-3xl bg-white rounded-lg shadow-lg p-8 animate-fade-in">
                <h2 className="text-3xl font-bold mb-6">Order Details</h2>
                <div className="grid grid-cols-2 gap-4">
                    <div>
                        <p className="text-gray-700">
                            <span className="font-bold">Order ID:</span>{" "}
                            {order.id}
                        </p>
                        <p className="text-gray-700">
                            <span className="font-bold">Customer:</span>{" "}
                            {order.customer}
                        </p>
                        <p className="text-gray-700">
                            <span className="font-bold">Product:</span>{" "}
                            {order.product}
                        </p>
                        <p className="text-gray-700">
                            <span className="font-bold">Quantity:</span>{" "}
                            {order.quantity}
                        </p>
                    </div>
                    <div>
                        <p className="text-gray-700">
                            <span className="font-bold">Total Price:</span> $
                            {order.totalPrice}
                        </p>
                        <p className="text-gray-700">
                            <span className="font-bold">Status:</span>{" "}
                            {order.status}
                        </p>
                        <p className="text-gray-700">
                            <span className="font-bold">Shipping Address:</span>{" "}
                            {order.shippingAddress}
                        </p>
                        <p className="text-gray-700">
                            <span className="font-bold">Order Date:</span>{" "}
                            {order.orderDate}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OrderDetails;

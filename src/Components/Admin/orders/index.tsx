"use client";
import Link from "next/link";
import React, { useState } from "react";
const ordersArray = [
    {
        id: 1,
        customer: "John Doe",
        product: "T-Shirt",
        quantity: 2,
        totalPrice: 39.98,
        status: "Pending",
        shippingAddress: "123 Main Street",
        orderDate: "2023-07-01",
        paymentMethod: "Credit Card",
        shippingMethod: "Express Shipping",
        isGift: false,
    },
    {
        id: 2,
        customer: "Jane Smith",
        product: "Sneakers",
        quantity: 1,
        totalPrice: 79.99,
        status: "Shipped",
        shippingAddress: "456 Elm Street",
        orderDate: "2023-06-29",
        paymentMethod: "PayPal",
        shippingMethod: "Standard Shipping",
        isGift: true,
    },
    {
        id: 3,
        customer: "David Johnson",
        product: "Backpack",
        quantity: 1,
        totalPrice: 39.99,
        status: "Delivered",
        shippingAddress: "789 Oak Street",
        orderDate: "2023-06-27",
        paymentMethod: "Credit Card",
        shippingMethod: "Express Shipping",
        isGift: false,
    },
    {
        id: 4,
        customer: "Sarah Williams",
        product: "Smartphone",
        quantity: 1,
        totalPrice: 599.99,
        status: "Pending",
        shippingAddress: "567 Pine Street",
        orderDate: "2023-06-25",
        paymentMethod: "Credit Card",
        shippingMethod: "Express Shipping",
        isGift: false,
    },
];

const Orders = () => {
    return (
        <div className="   p-6 container mx-auto ">
            <div className="relative !overflow-x-auto">
                <table className="table xl:table-fixed w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" className="px-6 py-3">
                                Product name
                            </th>
                            <th scope="col" className="px-6 py-3">
                                <div className="flex items-center">
                                    Quantity
                                    <a href="#">
                                        <svg
                                            className="w-3 h-3 ml-1.5"
                                            aria-hidden="true"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="currentColor"
                                            viewBox="0 0 24 24"
                                        >
                                            <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                                        </svg>
                                    </a>
                                </div>
                            </th>
                            <th scope="col" className="px-6 py-3">
                                <div className="flex items-center">
                                    Customer Name
                                    <a href="#">
                                        <svg
                                            className="w-3 h-3 ml-1.5"
                                            aria-hidden="true"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="currentColor"
                                            viewBox="0 0 24 24"
                                        >
                                            <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                                        </svg>
                                    </a>
                                </div>
                            </th>
                            <th scope="col" className="px-6 py-3">
                                <div className="flex items-center">
                                    Price
                                    <a href="#">
                                        <svg
                                            className="w-3 h-3 ml-1.5"
                                            aria-hidden="true"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="currentColor"
                                            viewBox="0 0 24 24"
                                        >
                                            <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                                        </svg>
                                    </a>
                                </div>
                            </th>
                            <th scope="col" className="px-6 py-3">
                                <span className="sr-only">Edit</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {ordersArray?.map((ele, ind) => (
                            <tr key={ind} className="bg-white dark:bg-gray-800">
                                <th
                                    scope="row"
                                    className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                                >
                                    {ele.product}
                                </th>

                                <td className="px-6 py-4">{ele.quantity}</td>
                                <td className="px-6 py-4">{ele.customer}</td>
                                <td className="px-6 py-4">${ele.totalPrice}</td>
                                <td className="px-6 py-4 flex justify-around text-right">
                                    <p className="font-medium text-blue-600 dark:text-blue-500 hover:underline">
                                        delete
                                    </p>

                                    <Link
                                        href={`/admin/orders/${ele.id}`}
                                        className="font-medium text-blue-600 dark:text-blue-500 hover:underline"
                                    >
                                        View
                                    </Link>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default Orders;

"use client";

import moment from "moment";
import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

interface MyObject {
    [key: string]: any;
}

interface State {
    options: ApexCharts.ApexOptions;
    series: any;
}

const Dashboard = () => {
    const [barLables, setBarLables] = useState([]);
    const [barData, setBarData] = useState([]);
    const [barLables2, setBarLables2] = useState([]);
    const [barData2, setBarData2] = useState([]);
    const [loading, setLoading] = useState(false);
    const [barSellsAmount, setBarSellsAmount] = useState([]);
    const [orderRatioLabels, setOrderRatioLabels] = useState([]);
    const [expireOrders, setExpireOrders] = useState({});
    const [sellsData, setSellsData] = useState({});
    const [allPackagesData, setAllPackagesData] = useState({});

    const state: State = {
        options: {
            chart: {
                id: "basic-line",
            },
            xaxis: {
                categories: ["A", "B", "C", "D", "E"],
            },
            colors: ["#5867c3", "#34c38f"],
            stroke: { curve: "smooth", width: 3 },
            grid: { borderColor: "#f1f1f1" },
            tooltip: { x: { format: "dd/MM/yy HH:mm" } },
        },

        series: [
            {
                name: "total sells",
                data: [8, 6, 3, 1, 7],
            },
        ],
    };

    const state2: State = {
        options: {
            chart: {
                id: "basic-line",
            },
            xaxis: {
                categories: ["AA", "BB", "CC"],
            },
            colors: ["#34c38f"],
            stroke: { curve: "smooth", width: 3 },
            grid: { borderColor: "#f1f1f1" },
            tooltip: { x: { format: "dd/MM/yy HH:mm" } },
        },

        series: [
            {
                name: "total sells",
                data: [50, 20, 32],
            },
        ],
    };

    const state3: State = {
        series: [
            {
                name: "Online",
                data: [55, 22],
            },
            {
                name: "Offline",
                data: [75, 10],
            },
        ],
        options: {
            chart: {
                type: "bar",
                height: 350,
                stacked: true,
                toolbar: {
                    show: true,
                },
                zoom: {
                    enabled: true,
                },
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        legend: {
                            position: "bottom",
                            offsetX: -10,
                            offsetY: 0,
                        },
                    },
                },
            ],
            plotOptions: {
                bar: {
                    horizontal: false,
                    borderRadius: 10,
                    dataLabels: {
                        total: {
                            enabled: true,
                            style: {
                                fontSize: "13px",
                                fontWeight: 900,
                            },
                        },
                    },
                },
            },
            xaxis: {
                categories: ["Pending Orders", "Complete Orders"],
            },
            legend: {
                position: "right",
                offsetY: 40,
            },
            fill: {
                opacity: 1,
            },
        },
    };

    const state4: State = {
        series: [75],
        options: {
            chart: {
                // height: 350,
                type: "radialBar",
                toolbar: {
                    show: true,
                },
            },
            plotOptions: {
                radialBar: {
                    startAngle: -135,
                    endAngle: 225,
                    hollow: {
                        margin: 0,
                        size: "70%",
                        background: "#fff",
                        image: undefined,
                        imageOffsetX: 0,
                        imageOffsetY: 0,
                        position: "front",
                        dropShadow: {
                            enabled: true,
                            top: 3,
                            left: 0,
                            blur: 4,
                            opacity: 0.24,
                        },
                    },
                    track: {
                        background: "#fff",
                        strokeWidth: "67%",
                        margin: 0, // margin is in pixels
                        dropShadow: {
                            enabled: true,
                            top: -3,
                            left: 0,
                            blur: 4,
                            opacity: 0.35,
                        },
                    },

                    dataLabels: {
                        show: true,
                        name: {
                            offsetY: -10,
                            show: true,
                            color: "#888",
                            fontSize: "17px",
                        },
                        value: {
                            color: "#111",
                            fontSize: "26px",
                            show: true,
                        },
                    },
                },
            },
            fill: {
                type: "gradient",
                gradient: {
                    shade: "dark",
                    type: "horizontal",
                    shadeIntensity: 0.5,
                    gradientToColors: ["#ABE5A1"],
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100],
                },
            },
            stroke: {
                lineCap: "round",
            },
            labels: ["Percent"],
        },
    };

    const state5: State = {
        series: [
            {
                name: "Package count",
                data: [20, 30, 10, 5, 14],
            },
        ],
        options: {
            chart: {
                type: "bar",
            },
            plotOptions: {
                bar: {
                    borderRadius: 3,
                    dataLabels: {
                        position: "top", // top, center, bottom
                    },
                },
            },

            xaxis: {
                categories: [
                    "100-200",
                    "200-300",
                    "300-400",
                    "400-500",
                    "500-600",
                ],
                position: "bottom",
                axisBorder: {
                    show: true,
                },
                axisTicks: {
                    show: false,
                },
                crosshairs: {
                    fill: {
                        type: "gradient",
                        gradient: {
                            colorFrom: "#D8E3F0",
                            colorTo: "#BED1E6",
                            stops: [0, 100],
                            opacityFrom: 0.4,
                            opacityTo: 0.5,
                        },
                    },
                },
                tooltip: {
                    enabled: true,
                },
            },
        },
    };

    const state6: State = {
        options: {
            chart: {
                id: "basic-pie",
            },
            labels: ["Mens", "Womens", "Childs"],
        },

        series: [50, 20, 79],
    };

    const state7: State = {
        series: [
            {
                name: "Online",
                data: [10, 20, 30],
            },
            {
                name: "Offline",
                data: [55, 13, 26],
            },
        ],
        options: {
            chart: {
                type: "bar",
                height: 350,
                stacked: true,
                toolbar: {
                    show: true,
                },
                zoom: {
                    enabled: true,
                },
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        legend: {
                            position: "bottom",
                            offsetX: -10,
                            offsetY: 0,
                        },
                    },
                },
            ],
            plotOptions: {
                bar: {
                    horizontal: false,
                    borderRadius: 10,
                    dataLabels: {
                        total: {
                            enabled: true,
                            style: {
                                fontSize: "13px",
                                fontWeight: 900,
                            },
                        },
                    },
                },
            },
            xaxis: {
                categories: ["Online Data", "Offline Data", "Others Data"],
            },
            legend: {
                position: "right",
                offsetY: 40,
            },
            fill: {
                opacity: 1,
            },
        },
    };

    const state8: State = {
        options: {
            chart: {
                id: "basic-pie",
            },
            labels: ["Online", "Offline"],
        },

        series: [52, 66],
    };

    return (
        <>
            <div className="container mx-auto p-6 grid grid-cols-1 lg:grid-cols-3 gap-4  ">
                <div className="lg:col-span-2 space-y-6 ">
                    <div className="flex flex-col lg:flex-row justify-between gap-6 ">
                        <div className="w-full bg-white    ">
                            <div className="h-full ">
                                <div className="  p-0">
                                    <div className="p-4">
                                        <div className=" flex justify-between ">
                                            <h5 className="  ">
                                                User order ratio
                                            </h5>
                                            <div className="">
                                                <p className="">lifetime</p>
                                            </div>
                                        </div>

                                        <Chart
                                            options={state4.options}
                                            series={state4.series}
                                            type="radialBar"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="w-full bg-white   ">
                            <div className="h-full">
                                <div className="  p-0">
                                    <div className="p-4">
                                        <div className="flex">
                                            <div className="flex-1">
                                                <h3 className="mb-3">
                                                    $
                                                    <span className=" ">
                                                        {55}
                                                    </span>
                                                </h3>
                                            </div>
                                            <div className="">
                                                <p className=" bg-primary   mb-0">
                                                    lifetime
                                                </p>
                                            </div>
                                        </div>
                                        <h5 className=" ">Total Sold</h5>
                                        <div className="h-[5px] mt-2"></div>
                                        <div className="flex">
                                            <div className="flex-1">
                                                <p className="mt-3  mb-0">
                                                    Since last month
                                                </p>
                                            </div>
                                            <div className="">
                                                <p className="text-success  mb-0 mt-3">
                                                    <i className=" "></i>
                                                    87.4 %
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className=" w-full bg-white  ">
                            <div className="h-full">
                                <div className=" p-0">
                                    <div className="p-4">
                                        <div className=" flex">
                                            <div className="flex-1">
                                                <h3 className="mb-3">
                                                    +
                                                    <span className=" ">
                                                        52
                                                    </span>
                                                </h3>
                                            </div>
                                            <div className="">
                                                <p className=" text-primary  ">
                                                    lifetime
                                                </p>
                                            </div>
                                        </div>
                                        <h5 className="  mb-0">Total Order</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="   bg-white p-4 mx-auto rounded-md ">
                        <div className="d-flex justify-content-between ">
                            <h3 className=" text-[17px] ">
                                Showing data on daily sells
                            </h3>
                            <div className="join">
                                <button className="btn join-item">Daily</button>
                                <button className="btn join-item">
                                    Monthly
                                </button>
                                <button className="btn join-item">
                                    Yearly
                                </button>
                            </div>
                        </div>
                        <Chart
                            options={state.options}
                            series={state.series}
                            type="area"
                            className="w-[100%]  "
                        />
                    </div>
                    <div className=" bg-white p-4 mx-auto rounded-md  ">
                        <h3 className=" text-[17px] ">
                            Packages in price range
                        </h3>
                        <Chart
                            options={state5.options}
                            series={state5.series}
                            type="bar"
                            className=" w-[100%]   "
                        />
                    </div>
                    <div className=" bg-white p-4 mx-auto rounded-md ">
                        <h3 className=" text-[17px] ">Order expires in days</h3>
                        <Chart
                            options={state7.options}
                            series={state7.series}
                            type="bar"
                            className=" w-full"
                        />
                    </div>
                </div>

                <div className=" space-y-6 ">
                    <div className=" bg-white p-4 mx-auto rounded-md ">
                        <div className="d-flex justify-content-between ">
                            <h3 className=" text-[13px] ">
                                Showing data on Yarly customer
                            </h3>
                            <div className="join   ">
                                <button className="btn join-item">Daily</button>
                                <button className="btn join-item">
                                    Montly
                                </button>
                                <button className="btn join-item">
                                    Yearly
                                </button>
                            </div>
                        </div>

                        <Chart
                            options={state2.options}
                            series={state2.series}
                            type="area"
                            className=" w-full"
                        />
                    </div>
                    <div className=" bg-white p-4 mx-auto rounded-md ">
                        <h3 className=" text-[17px] ">
                            Pending vs complate order
                        </h3>
                        <Chart
                            options={state3.options}
                            series={state3.series}
                            type="bar"
                            className=" w-full"
                        />
                    </div>
                    <div className=" bg-white p-4 mx-auto rounded-md    ">
                        <h3 className=" text-[17px] ">Payment Status Ratio</h3>
                        <Chart
                            options={state6.options}
                            series={state6.series}
                            type="pie"
                            className=" w-[100%]   "
                        />
                    </div>

                    <div className=" bg-white p-4 mx-auto rounded-md    ">
                        <h3 className=" text-[17px] ">Ip based order ratio</h3>
                        <Chart
                            options={state8.options}
                            series={state8.series}
                            type="pie"
                            className=" w-[100%]   "
                        />
                    </div>
                </div>
            </div>
        </>
    );
};

export default Dashboard;

"use client";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import React from "react";

const LeftSidebar = () => {
    const router = usePathname();

    return (
        <ul className="menu p-4 w-80 space-y-6 h-full bg-base-200">
            <li
                className={`${router == "/admin" && "bg-[#DCDDDE] rounded-md"}`}
            >
                <Link href="/admin">Dashboard</Link>
            </li>
            <li
                className={`${
                    router.includes("/admin/products") &&
                    "bg-[#DCDDDE] rounded-md"
                }`}
            >
                <Link href="/admin/products">Products</Link>
            </li>
            <li
                className={`${
                    router.includes("/admin/customers") &&
                    "bg-[#DCDDDE] rounded-md"
                }`}
            >
                <Link href="/admin/customers">customers</Link>
            </li>
            <li
                className={`${
                    router.includes("/admin/orders") &&
                    "bg-[#DCDDDE] rounded-md"
                }`}
            >
                <Link href="/admin/orders">orders</Link>
            </li>
        </ul>
    );
};

export default LeftSidebar;
